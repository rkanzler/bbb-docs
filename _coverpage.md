<!-- https://gitlab.ikmz.europa-uni.de/rkanzler/bbb-docs/-/raw/master/_coverpage.md -->

![logo](https://gitlab.ikmz.europa-uni.de/rkanzler/bbb-docs/-/raw/master/_media/viadrina-logo.svg)

# BigBlueButton

> Open-Source-Webkonferenzsystem für die Lehre

[Login](https://bbb.europa-uni.de/)
[Dokumentation](#bbb-dokumentation-an-der-viadrina)