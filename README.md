# BBB Dokumentation an der Viadrina


!> **Achtung:** Diese Dokumentation ist noch nicht vollständig und wird  kontinuierlich ausgebaut. Es lohnt sich, regelmäßig diese Seite aufzurufen.

!> **Allgemein:** BigBlueButon ist derzeit im Beta-Betrieb. Im Hintergrund läuft ein Pool an BBB-Servern, die über einen Load-Balancer zugewiesen werden. Nur durch diesen Live-Beta-Betrieb können wir erfahren, wo noch Probleme liegen und was wir weiter verbessern müssen. Das System lebt von Feedback – und wir hoffen, dass wir Stück für Stück durch diese herausfordernde Zeit kommen.

## Häufig gestellte Fragen & Antworten zu den Themen Allgemeines, Internetverbindung & Fehler

* [Für welche Lehrszenarien ist BigBlueButton gedacht?](#für-welche-lehrszenarien-ist-bigbluebutton-gedacht)
* [Warum gibt es zwei verschiedene Konferenzsysteme? Für welche Szenarien soll ich BigBlueButton nutzen, für welche Jitsi?](#warum-gibt-es-zwei-verschiedene-konferenzsysteme-für-welche-szenarien-soll-ich-bigbluebutton-nutzen-für-welche-jitsi)
* [Wie kann ich feststellen, ob mein Computer und meine Internetverbindung ausreichend gut ist für BigBlueButton?](#wie-kann-ich-feststellen-ob-mein-computer-und-meine-internetverbindung-ausreichend-gut-ist-für-bigbluebutton)
* [Meine Internetverbindung ist leider sehr schlecht. Wie kann ich trotzdem an BigBlueButton Meetings teilnehmen?](#meine-internetverbindung-ist-leider-sehr-schlecht-wie-kann-ich-trotzdem-an-bigbluebutton-meetings-teilnehmen)
* [Sie empfehlen für die Nutzung aktuell nur den Webbrowser Google Chrome. Kann ich keinen anderen Browser nutzen?](#sie-empfehlen-für-die-nutzung-aktuell-nur-den-webbrowser-google-chrome-kann-ich-keinen-anderen-browser-nutzen)
* [BigBlueButton kann nicht auf mein Mikrofon / Kamera zugreifen. An was kann das liegen oder was soll ich tun?](#bigbluebutton-kann-nicht-auf-mein-mikrofon-kamera-zugreifen-an-was-kann-das-liegen-oder-was-soll-ich-tun)

### Warum gibt es zwei verschiedene Konferenzsysteme? Für welche Szenarien soll ich BigBlueButton nutzen, für welche Jitsi?
Wir stellen Ihnen zwei verschiedene virtuelle Konferenzsysteme zur Verfügung, um verschiedene Bedarfe abdecken zu können und für jedes Szenario die passendste Lösung anbieten zu können. Jedes System hat unterschiedliche Stärken und Schwächen.
Wir haben Ihnen nachfolgend ein Entscheidungsdiagramm erstellt, welches Ihnen bei der Wahl des richtigen Systems helfen soll.

![logo](https://gitlab.ikmz.europa-uni.de/rkanzler/bbb-docs/-/raw/master/_media/entscheidungsdiagramm.png)


### Für welche Lehrszenarien ist BigBlueButton gedacht?
BigBlueButton wird **für Seminare und andere interaktive Veranstaltungsformate, mit einer überschaubaren Teilnehmerzahl** zur Verfügung gestellt. Für die Teilnehmerzahl sehen Sie bitte auch die Frage "Wie viele Personen können an einem BigBlueButton Meeting teilnehmen?"

* Es dient NICHT zum Live-Streaming großer Veranstaltungen. 
* Es dient NICHT für die Kommunikation in Teams oder mit Einzelpersonen (z.B. Individualsprechstunden ohne Lehrbezug). Nehmen Sie in diesem Fall bitte unsere Lösung [Jitsi](https://jitsi01.europa-uni.de).

### Wie kann ich feststellen, ob mein Computer und meine Internetverbindung ausreichend gut ist für BigBlueButton?

#### Internetverbindung
Ihre Internetverbindung können Sie zum Beispiel mit dem "[Belwue Speedtest](http://speedtest.belwue.net/browser-speedtest/ "BelWü Speedtest")" testen.  
Hier sollten die Werte für "_Download_" (mindestens 1 MBit/s | ideal 5 MBit/s) und "_Upload_" (mindestens 0.5 MBit/s | ideal 1 MBit/s) möglichst groß sein, während "_Ping_" und "_Jitter_" möglichst kleine Werte erreichen sollten.  

**Unsere Empfehlung:**  
Verbinden Sie Ihren Computer am besten direkt mittels **Kabel** an den Router (**LAN**). Damit können Sie sicherstellen, dass nicht ein von zu vielen Nutzenden überlaufenes WLAN Ihnen gerade Verbindungsprobleme bereitet.

#### Browser
Ob Ihr Browser die technologischen Basis WebRTC "sprechen" kann, erfahren Sie mit dem [WebRTC-Test](https://test.webrtc.org/ "WebRTC Test").  

**Unsere Empfehlung:**  
Nutzen Sie die Browser **Mozilla** **Firefox** oder **Google** **Chrome** in einer aktuellen Version. Damit funktioniert BBB aus Erfahrung sehr gut.

### Meine Internetverbindung ist leider sehr schlecht. Wie kann ich trotzdem an BigBlueButton Meetings teilnehmen?
Falls Sie alle Tipps und Hilfestellungen zur Verbesserung der Verbindung umgesetzt haben, aber trotzdem noch Probleme im Meeting haben sollten, können Sie versuchen, die Einstellungen zur Datenvolumeneinsparung von BBB zu nutzen.

Sie finden diese im Hauptfenster des Meetingraums rechts oben hinter dem **Icon mit den drei Punkten** im **Menüeintrag Einstellungen** öffnen (Abb. 1).
Gehen Sie dann in den Abschnitt **Datenvolumeneinsparungen**. Dort können Sie zum Beispiel dann die Webcams aller Nutzer/innen für Sie abschalten (Abb. 2).
![logo](https://gitlab.ikmz.europa-uni.de/rkanzler/bbb-docs/-/raw/master/_media/abb2.png)

### Sie empfehlen für die Nutzung aktuell nur den Webbrowser Google Chrome. Kann ich keinen anderen Browser nutzen?
Gerne würden wir Ihnen mehr als nur einen Webbrowser als Empfehlung angeben. Momentan ist die Lage aber so, dass die Funktionalität von BigBlueButton am Besten mit Googles Webbrowser Chrome oder seinem Open-Source Derivat Chromium funktioniert und wir diese(n) daher für die Nutzung explizit empfehlen.

Neben dieser offiziellen Empfehlung können Sie natürlich auch andere Browser für die Nutzung mit BigBlueButton ausprobieren. Insbesondere Mozilla Firefox wird häufig auch gut funktionieren. Hier konnten wir aber in einigen Fällen beobachten, dass sich die Audioqualität deutlich schlechter als mit der Nutzung von Chrome dargestellt hat. In einigen anderen Browsern wiederum funktioniert beispielsweise die Bildschirmfreigabe nicht (siehe auch "Ich finde den Button zur Bildschirmfreigabe nicht. Warum erscheint er bei mir nicht?").

Sollten Sie einen anderen Browser als Chrome nutzen und auf Fehler oder Probleme stoßen, probieren Sie bitte zuerst einmal aus, ob sich dieses Verhalten mit dem Webbrowser Chrome ebenfalls zeigt. Falls sich die Probleme damit nicht zeigen, dann folgen Sie bitte unserer Empfehlung und nutzen diesen Browser für Ihre BigBlueButton Meetings.

Sollten sich die Probleme auch mit Chrome zeigen, dann kontaktieren Sie uns damit sehr gerne über den Helpdesk.

### BigBlueButton kann nicht auf mein Mikrofon / Kamera zugreifen. An was kann das liegen oder was soll ich tun?
Folgende Stolpersteine / Ansatzpunkte zur Behebung des Problems kann es geben:

*   Nutzen Sie bereits die von uns empfohlene Browser **Google Chrome** oder **Chromium**? Falls nicht, bitte probieren Sie zuerst aus, ob sich die Probleme mit diesen Browsern auch darstellen.  

*   Kann BigBlueButton oder eine andere Software (Skype oder WebEx) Ihre Geräte überhaupt finden? Wenn nicht, prüfen Sie bitte, ob Sie die nötigen **Treiber** installiert haben. In einzelnen Fällen konnten auch Probleme mit einem Treiberupdate behoben werden. Sollte es so ewtas für Ihre Geräte geben, dann können Sie das zur Sicherheit auch auspobieren.  

*   Kann es sein, dass noch andere Software (Skype oder WebEx) oder ein anderes Meeting in einem anderen Browser / Browsertab exklusiven Zugriff auf die Geräte hat? Falls ja, dann **schließen Sie bitte alle anderen virtuellen Meetings**.  

*   Wurde **dem Browser** die **Zustimmung gegeben** beziehungsweise vielleicht versehentlich nicht erlaubt, **auf Mikro und Kamera zuzugreifen**? Sie können das in Google Chrome schnell über das Schlossicon links direkt neben der Adresszeile überprüfen und falls die Zustimmung nicht erteilt wurde, dies dort ändern.  

*   **Wurde die richtige Ton- und Bildquelle ausgewählt**? Spätestens sobald Sie ein externes Headset anschließen, haben Sie mindestens zwei Quellen für Audioausgabe und mindestens eine für Mikrofon. Hier muss dann drauf geachtet werden, die richtige Quelle jeweils auszuwählen.  
    Falls Sie sich unsicher sind, was Sie in BigBlueButton ausgewählt haben: Im Echo Test können Sie das rote Icon mit dem Daumen nach unten anklicken und danach erscheint ein Dialog, in dem Sie die Quellen neu auswählen können.  

*   Nutzen Sie ein Notebook oder ein externes Headset? Manche Headsets und Notebooks haben nochmal einen **eigenen Stummschalter**, mit dem man hardwareseitig das Mikro stummschalten kann. Bitte überprüfen Sie das entsprechend.  

*   Sollten Sie mit all diesen Hilfestellungen keine Verbindung mit Ihrem Mikrofon erhalten aber ein BigBlueButton Meeting für Sie unmittelbar bevorstehen, können Sie als Ausweichlösung auch die Telefonintegration (siehe FAQs unten) nutzen. Dies ist allerdings **keine Dauerlösung** und Sie müssen Ihren lokalen Mikrofonproblemen weiterhin auf den Grund gehen.



-------------------------
### Anmeldung

Grundsätzlich hat jeder Angehörige der Europa-Universität Viadrina die Möglichkeit sich über bbb.europa-uni.de mit seinen Uni Account anzumelden.

Grundsätze und Grundlagen
-------------------------

-   Räume lassen sich über das Zahnrad an der Teilnehmerliste →
    *Zuschauerrechte einschränken* weiter konfigurieren.
-   Vorher abgesprochenes gemeinsames Verständnis der
    Audionutzung/Mutefunktion ist besser, als zu versuchen, dies
    technisch einzuschränken
-   Die Mute-Funktion sollte von allen genutzt werden, um mögliche
    Hintergrundgeräusche nicht zu übertragen. Dies hilft bei der
    Verständlichkeit und sorgt für bessere Übertragung des Tons der
    gerade sprechenden Person.


Breakout Rooms
--------------

-   Sind wirklich nur temporäre Räume
-   Schließen sich automatisch nach der eingestellten Zeit
    -   \*Ergebnisse und Materialien werden danach **nicht** in den
        Hauptraum zurückgeführt!\*
-   Alle TeilnehmerInnen können sich selbst PräsentatorInnenrechte
    geben; die erste Person, die dem Raum beitritt, hat
    ModeratorInnenrechte. Die Räume eignen sich im derzeitigen Zustand
    deshalb wirklich nur für die Kleingruppenarbeit, wenn die
    Teilnehmenden diszipliniert mit dem Setup umgehen können. Wir
    wissen, dass das im Schulkontext manchmal schwierig wird :-x

Arbeitsbereich
--------------

-   Bearbeitungen auf den Präsentationen sind temporär und können nicht
    abgespeichert werden.

Dateien bereitstellen für Teilnehmer
---------------------------------

-   Geht nicht in BBB
-   Möglichkeit: Anderswo (Moodle?) hochladen und Downloadlink in
    öffentlichen Chat posten

Peripheriegeräte iPad / Tablet
------------------------------

-   Lassen sich nicht direkt koppeln.
-   Können als weiterer Teilnehmer (dann Moderations/Präsentationsrechte
    vergeben) genutzt werden, um auf dem BBB-Whiteboard zu zeichnen.
-   Eine Bildschirmfreigabe von iPads ist technisch leider nicht einfach
    möglich. iOS bietet aktuell keine Möglichkeit, BBB als
    "Browseranwendung" den Bildschirminhalt bereitzustellen.

     * Mit diesen folgenden aufwändigeren, **experimentellen** Möglichkeiten lässt sich der Bildschirm des iPads dann als Fenster am Rechner darstellen, dieses Fenster kann dann in BBB geteilt werden. : 
       * Mac mit Quicktime Player [Anleitung auf frisch-gebloggt.de](https://www.frisch-gebloggt.de/itgadgets/mit-quicktime-den-ios-bildschirm-auf-dem-computer-anzeigen-aufnehmen/)
       * [AirServer](https://www.airserver.com/) (auch unter Windows, Kaufsoftware)
       * [Reflector](https://www.airsquirrels.com/reflector) (Kaufsoftware)
       * [LetsView](https://letsview.com/) (kostenlos)

     * Dies sind, wie geschrieben, experimentelle Lösungen, sollten also __vor__ einem Call ausgiebig getestet werden.
     

Webcam-Alternativen?
--------------------

-   Den Raum als Teilnehmer zusätzlich auch mit dem Smartphone betreten,
    Video freigeben
-   Das eigene Smartphone als Webcam am Rechner verwenden:
    <https://www.theverge.com/2020/4/20/21225061/phone-tablet-android-ios-phone-tablet-pc-webcam-windows-mac-os-how-to-use>


Mithelfen und Credits
=====================

Die BBB Umgebung an der Viadrina steht, wie viele andere Dienste, auf den Schultern von vielen. Es basiert maßgeblich auf folgender Open-Source-Software, die von allen frei verwendbar, der Code einseh- und auch selbst anpassbar und mitgestaltbar ist:

-   [BigBlueButton] - der eigentlichen Konferenzsoftware und ihre
    Komponenten:
    -   [FreeSWITCH] - als Audioübertragungslösung
    -   [Kurento] Media Server - als Videoübertragungslösung
    -   [coturn] - als Audio/Video-Relay
-   [Greenlight] - als Raummanagementsystem
-   [scalelite] - als Lastverteilungssystem
-   [Prometheus] - für das Monitoring
    -   [bbb-exporter] - für Statistiken aus BBB
-   [Grafana] - Dashboard und Visualisierung des Monitorings
-   [Docsify] - dieses Dokumentation

  [BigBlueButton]: https://github.com/bigbluebutton/bigbluebutton
  [FreeSWITCH]: https://freeswitch.com
  [Kurento]: https://github.com/Kurento/kurento-media-server
  [coturn]: https://github.com/coturn/coturn
  [Greenlight]: https://github.com/bigbluebutton/greenlight
  [scalelite]: https://github.com/blindsidenetworks/scalelite
  [Prometheus]: https://prometheus.io
  [bbb-exporter]: https://github.com/greenstatic/bigbluebutton-exporter
  [Grafana]: http://grafana.com
  [Docsify]: https://github.com/docsifyjs/docsify/
